<?php

namespace App\Http\Models;

use Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class PurchaseModel extends Model {
	public $timestamps      = false;
  const CREATED_AT        = 'time_created';
  const UPDATED_AT        = 'last_update';

	protected $table 	      = 'tb_purchase';
	protected $primaryKey   = 'id_purchase';
	protected $fillable     = ['id_purchase', 'id_restaurant','purchase_code','time_created','last_update','status','id_user','payment_callback','payment_method','payment_status'];

  public function insertData($data){
    return PurchaseModel::create($data)->id_purchase;
  }

  public function updateData($data){
    return PurchaseModel::where("id_purchase",$data['id_purchase'])->update($data);
  }

  public function getDetailpurchase($purchase_code){
    return PurchaseModel::where("purchase_code",$purchase_code)
          ->join(DB::raw("(SELECT GROUP_CONCAT(CONCAT(quantity,'x ',name,' @',tb_restaurant_menu.price,'USD ') SEPARATOR '|') as items, SUM(tb_purchase_detail.price*quantity) as subtotal, id_purchase FROM tb_purchase_detail, tb_restaurant_menu WHERE tb_purchase_detail.id_menu = tb_restaurant_menu.id_menu GROUP by id_purchase )detail"),"detail.id_purchase","=",$this->table.".id_purchase")
          ->first();
  }

  public function getListofRestaurantTransactions($id_restaurant){
    return PurchaseModel::where("id_restaurant",$id_restaurant)
          ->join(DB::raw("(SELECT GROUP_CONCAT(CONCAT(quantity,'x ',name,' @',tb_restaurant_menu.price,'USD ') SEPARATOR '|') as items, SUM(tb_purchase_detail.price*quantity) as subtotal, id_purchase FROM tb_purchase_detail, tb_restaurant_menu WHERE tb_purchase_detail.id_menu = tb_restaurant_menu.id_menu GROUP by id_purchase )detail"),"detail.id_purchase","=",$this->table.".id_purchase")
          ->join(DB::raw("(SELECT first_name, last_name, id_user FROM tb_user)user"),"user.id_user","=",$this->table.".id_user")
          ->orderBy("time_created","DESC")
          ->get();
  }

  public function getListofUsersTransactions($id_user){
    return PurchaseModel::where("id_user",$id_user)
          ->join(DB::raw("(SELECT GROUP_CONCAT(CONCAT(quantity,'x ',name,' @',tb_restaurant_menu.price,'USD ') SEPARATOR '|') as items, SUM(tb_purchase_detail.price*quantity) as subtotal, id_purchase FROM tb_purchase_detail, tb_restaurant_menu WHERE tb_purchase_detail.id_menu = tb_restaurant_menu.id_menu GROUP by id_purchase )detail"),"detail.id_purchase","=",$this->table.".id_purchase")
          ->join(DB::raw("(SELECT name as name_of_restaurant, id_restaurant FROM tb_restaurant)rest"),"rest.id_restaurant","=",$this->table.".id_restaurant")
          ->orderBy("time_created","DESC")
          ->get();
  }

}
