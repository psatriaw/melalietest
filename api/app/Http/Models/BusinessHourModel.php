<?php

namespace App\Http\Models;

use Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class BusinessHourModel extends Model {
	public $timestamps      = false;
  const CREATED_AT        = 'time_created';
  const UPDATED_AT        = 'last_update';

	protected $table 	      = 'tb_restaurant_business_hour';
	protected $primaryKey   = 'id_business_hour';
	protected $fillable     = ['id_business_hour', 'id_restaurant','day','open_hour','time_created','last_update','status','close_hour'];

  public function getListRestaurantByData($date){
    return BusinessHourModel::orderBy('name')->get();
  }

  public function insertData($data){
    return BusinessHourModel::create($data)->id_business_hour;
  }
}
