<?php

namespace App\Http\Models;

use Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class PurchaseDetailModel extends Model {
	public $timestamps      = false;
  const CREATED_AT        = 'time_created';
  const UPDATED_AT        = 'last_update';

	protected $table 	      = 'tb_purchase_detail';
	protected $primaryKey   = 'id_purchase_detail';
	protected $fillable     = ['id_purchase_detail', 'id_purchase','id_menu','time_created','last_update','status','price','quantity','discount'];

  public function insertData($data){
    return PurchaseDetailModel::create($data)->id_purchase_detail;
  }

}
