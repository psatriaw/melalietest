<?php

namespace App\Http\Models;

use Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class UserModel extends Model {
	public $timestamps      = false;
  const CREATED_AT        = 'time_created';
  const UPDATED_AT        = 'last_update';

	protected $table 	      = 'tb_user';
	protected $primaryKey   = 'id_user';
	protected $fillable     = ['id_user', 'first_name','longitude','latitude','time_created','last_update','status','first_name','last_name','username','password','balance','otp','otp_valid'];

  public function getListRestaurantByData($date){
    return UserModel::orderBy('name')->get();
  }

  public function insertData($data){
    return UserModel::create($data)->id_user;
  }

  public function updateData($data){
    return UserModel::where("id_user",$data['id_user'])->update($data);
  }

  public function getDetail($id_user){
    return UserModel::where("id_user",$id_user)->first();
  }

  public function getTopUserByTransactionAmount($min_date, $max_date){
    $min_date = strtotime($min_date);
    $max_date = strtotime($max_date);

    $data = UserModel::where("status","active")
            ->leftjoin(DB::raw("(SELECT SUM(price) as total_transactions, id_user FROM tb_purchase_detail, tb_purchase WHERE (tb_purchase.time_created BETWEEN $min_date AND $max_date) AND  tb_purchase_detail.id_purchase = tb_purchase.id_purchase GROUP BY id_user)pc"),"pc.id_user","=",$this->table.".id_user")
            ->orderBy("total_transactions","DESC")
            ->skip(0)
            ->limit(10)
            ->get();
    return $data;
  }

  public function getUserByTransactionAmountWithMinimumTrx($min_date, $max_date, $min_trx, $limit){
    $min_date = strtotime($min_date);
    $max_date = strtotime($max_date);

    $data = UserModel::where("status","active")
            ->leftjoin(DB::raw("(SELECT SUM(price) as total_transactions, id_user FROM tb_purchase_detail, tb_purchase WHERE (tb_purchase.time_created BETWEEN $min_date AND $max_date) AND  tb_purchase_detail.id_purchase = tb_purchase.id_purchase GROUP BY id_user)pc"),"pc.id_user","=",$this->table.".id_user")
            ->orderBy("total_transactions","DESC")
            ->where("total_transactions","<",$min_trx)
            ->skip(0)
            ->limit($limit)
            ->get();
    return $data;
  }
}
