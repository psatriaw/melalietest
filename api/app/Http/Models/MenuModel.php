<?php

namespace App\Http\Models;

use Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class MenuModel extends Model {
	public $timestamps      = false;
  const CREATED_AT        = 'time_created';
  const UPDATED_AT        = 'last_update';

	protected $table 	      = 'tb_restaurant_menu';
	protected $primaryKey   = 'id_menu';
	protected $fillable     = ['id_menu', 'id_restaurant','name','time_created','last_update','status','price'];

  public function getMenuByNameOnRestaurant($restaurant_name,$nama_menu){
    $data = MenuModel::where("status",'active')
            ->join(DB::raw("(SELECT id_restaurant FROM tb_restaurant WHERE name = '$restaurant_name')rest"),"rest.id_restaurant","=",$this->table.".id_restaurant")
            ->where('name',$nama_menu)
            ->first();
    if($data){
      return $data->id_menu;
    }else{
      return 0;
    }
  }

  public function insertData($data){
    return MenuModel::create($data)->id_menu;
  }

  public function getNameOfrestaurantWhoHavethisItemMenu($id_menu){
    $data = MenuModel::where("id_menu",$id_menu)->first();
    return $data->id_restaurant;
  }

}
