<?php

namespace App\Http\Models;

use Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class RestaurantModel extends Model {
	public $timestamps      = false;
  const CREATED_AT        = 'time_created';
  const UPDATED_AT        = 'last_update';

	protected $table 	      = 'tb_restaurant';
	protected $primaryKey   = 'id_restaurant';
	protected $fillable     = ['id_restaurant', 'name','longitude','latitude','time_created','last_update','status','id_owner','balance'];

  public function getListRestaurantByOpenDate($day, $time){
    return RestaurantModel::where("status","active")
           ->join(DB::raw("(SELECT id_restaurant as id_rest FROM tb_restaurant_business_hour WHERE day = '$day' AND (close_hour >= '$time' AND open_hour <= '$time'))rest"),"rest.id_rest","=",$this->table.".id_restaurant")
           ->orderBy('name')
           ->get();
  }

  public function getListofRestaurantByLocation($latitude, $longitude){
		$raw 			= DB::raw("SELECT name, longitude, latitude, (6371 * acos( cos( radians(" . $latitude . ") ) * cos( radians( `latitude` ) ) * cos( radians(`longitude` ) - radians(" . $longitude . ") ) + sin( radians(" .$latitude. ") ) * sin( radians(`latitude`) ) ) ) as distance FROM tb_restaurant WHERE status = 'active' ORDER BY distance ASC LIMIT 0,10");
		$data 		= DB::select($raw);
		return $data;
	}

  public function getListofRestaurantByNumberHour($hours){
    return RestaurantModel::where("status","active")
           ->select("*","hours_per_day","day","open_hour","close_hour")
           ->join(DB::raw("(SELECT TIMESTAMPDIFF(HOUR, open_hour, close_hour) AS `hours_per_day`, open_hour, close_hour, day, id_restaurant as id_rest FROM tb_restaurant_business_hour WHERE TIMESTAMPDIFF(HOUR, open_hour, close_hour) >= $hours)rest"),"rest.id_rest","=",$this->table.".id_restaurant")
           ->orderBy('name')
           ->get();
  }

  public function getListofRestaurantByDhisesPrice($min_price, $max_price, $number_of_dishes){
    return RestaurantModel::where("status","active")
           ->select("*","total_dishes_around_price")
           ->join(DB::raw("(SELECT COUNT(*) total_dishes_around_price, id_restaurant as id_rest FROM tb_restaurant_menu WHERE (price BETWEEN $min_price AND $max_price) AND status = 'active' GROUP BY id_rest)menu"),"menu.id_rest","=",$this->table.".id_restaurant")
           ->orderBy('name')
           ->where("total_dishes_around_price",">",$number_of_dishes)
           ->get();
  }

  public function getListofRestaurantByTransactionVolume($limit){
    $data = RestaurantModel::where("status","active")
            ->leftjoin(DB::raw("(SELECT SUM(price) as volume_transactions, COUNT(*) as total_transactions, id_restaurant as id_rest FROM tb_purchase_detail, tb_purchase WHERE tb_purchase_detail.id_purchase = tb_purchase.id_purchase GROUP BY id_rest)pc"),"pc.id_rest","=",$this->table.".id_restaurant")
            ->orderBy("total_transactions","DESC")
            ->orderBy("volume_transactions","DESC")
            ->skip(0)
            ->limit($limit)
            ->get();
    return $data;
  }

  public function getListofRestaurantByDishesName($keyword){
    $menus = MenuModel::where("name",$keyword)
             ->join(DB::raw("(SELECT name as restaurant_name,longitude, latitude, id_restaurant FROM tb_restaurant)rest"),"rest.id_restaurant","=","tb_restaurant_menu.id_restaurant")
             ->get();

    return $menus;
  }

  public function getListofRestaurantByDishesRelevance($keywords){

    $menus = MenuModel::where(function($query) use ($keywords){
               foreach ($keywords as $key => $value) {
                  if($key==0){
                    $query->where("name","like","%".$value."%");
                  }else{
                    $query->orwhere("name","like","%".$value."%");
                  }
                }
             })
             ->join(DB::raw("(SELECT name as restaurant_name, longitude, latitude, id_restaurant FROM tb_restaurant)rest"),"rest.id_restaurant","=","tb_restaurant_menu.id_restaurant")
             ->get();

    return $menus;
  }

  public function getIDByName($name){
    $data  = RestaurantModel::where("name",$name)->first();
    if($data){
      return $data->id_restaurant;
    }else{
      return 0;
    }
  }

  public function insertData($data){
    return RestaurantModel::create($data)->id_restaurant;
  }
}
