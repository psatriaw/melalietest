<?php

namespace App\Http\Controllers;

//===============================
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;

use App\Http\Controllers\Controller;

use App\Http\Models\RestaurantModel;
use App\Http\Models\UserModel;
use App\Http\Models\BusinessHourModel;
use App\Http\Models\MenuModel;
use App\Http\Models\PurchaseModel;
use App\Http\Models\PurchaseDetailModel;

class ApiController extends Controller{
  protected $restaurant_model;
  protected $user_model;

  public function __construct(){
    $this->restaurant_model       = new RestaurantModel();
    $this->user_model             = new UserModel();
    $this->business_hour_model    = new BusinessHourModel();
    $this->menu_model             = new MenuModel();
    $this->purchase_model         = new PurchaseModel();
    $this->purchase_detail_model  = new PurchaseDetailModel();
  }

  public function getMenuByNameOnRestaurant(Request $request){
    $nama_menu  = $request->nama_menu;
    $nama_resto = $request->nama_resto;
    $id_menu   = $this->purchase_model->getDetailpurchase("17953596410");
    print_r($id_menu);
  }

  public function extractdatabase(){

    $content              = file_get_contents("uploads/restaurants.json");
    $data_restaurant      = json_decode($content,true);

    foreach ($data_restaurant as $key => $value) {
      $location   = $value['location'];
      $location   = explode(",",$location);
      $longitude  = $location[1];
      $latitude   = $location[0];
      $balance    = $value['balance'];

      $datarestaurant = array(
        'name'          => $value['name'],
        'longitude'     => $longitude,
        'latitude'      => $latitude,
        'time_created'  => time(),
        'last_update'   => time(),
        'status'        => 'active',
        'balance'       => $balance
      );

      $restaurant  = $this->restaurant_model->insertData($datarestaurant);
      if($restaurant){
        $id_restaurant = $restaurant;

        //insert business hour
        $business_hours = $value['business_hours'];
        if($business_hours!=""){
          $business_hours_array = explode("|",$business_hours);
          foreach ($business_hours_array as $key2 => $value2) {
            $business_time = explode(": ",$value2);
            $days          = explode(",",$business_time[0]);

            $business_hour = explode(" - ",@$business_time[1]);

            foreach ($days as $keys => $values) {
              $day        = date("l",strtotime($values));
              $open_hour  = date("H:i", strtotime($business_hour[0]));
              $close_hour = date("H:i", strtotime($business_hour[1]));

              $databusineshour = array(
                "time_created"  => time(),
                "last_update"   => time(),
                "status"        => 'active',
                "id_restaurant" => $id_restaurant,
                "day"           => $day,
                "open_hour"     => $open_hour,
                "close_hour"    => $close_hour
              );

              $this->business_hour_model->insertData($databusineshour);
            }
          }
        }
        //

        //insert menus
        $menu = $value['menu'];
        foreach ($menu as $key2 => $value2) {
          $datamenu = array(
            "time_created"  => time(),
            "last_update"   => time(),
            "status"        => 'active',
            "id_restaurant" => $id_restaurant,
            "name"          => $value2['name'],
            "price"         => $value2['price']
          );

          $this->menu_model->insertData($datamenu);

        }
        //

      }
    }


    $content  = file_get_contents("uploads/users.json");
    $data     = json_decode($content,true);

    foreach ($data as $key => $value) {
      $location   = $value['location'];
      $location   = explode(",",$location);
      $longitude  = $location[1];
      $latitude   = $location[0];
      $balance    = $value['balance'];
      $name       = explode(" ",$value['name']);
      $first_name = $name[0];
      $last_name  = @$name[1];

      $user = array(
        "time_created"  => time(),
        "last_update"   => time(),
        "status"        => "active",
        "longitude"     => $longitude,
        "latitude"      => $latitude,
        "first_name"    => $first_name,
        "last_name"     => $last_name,
        "balance"       => $balance
      );

      //insert data to user table
      $user = $this->user_model->insertData($user);

      $purchases = $value['purchases'];
      foreach ($purchases as $key2 => $value2) {
        $nama_menu        = $value2['dish'];
        $restaurant_name  = $value2['restaurant_name'];
        $id_menu          = $this->menu_model->getMenuByNameOnRestaurant($restaurant_name,$nama_menu);

        $id_restaurant    = $this->menu_model->getNameOfrestaurantWhoHavethisItemMenu($id_menu);
        $amount           = $value2['amount'];
        $date             = $value2['date'];
        $id_user          = $user;

        //assumes that 1 detail for 1 transaction

        $datapurchase     = array(
          "time_created"  => strtotime($date),
          "last_update"   => strtotime($date),
          "status"        => "active",
          "purchase_code" => time().$key2,
          "id_user"       => $id_user,
          "id_restaurant" => $id_restaurant
        );

        $purchase = $this->purchase_model->insertData($datapurchase);


        $datapurchasesitem    = array(
          "time_created"  => strtotime($date),
          "last_update"   => strtotime($date),
          "status"        => "active",
          "id_purchase"   => $purchase,
          "id_menu"       => $id_menu,
          "price"         => $amount,
          "quantity"      => 1  //assumes it is 1
        );

        $this->purchase_detail_model->insertData($datapurchasesitem);
      }

    }
  }

  public function getlistrestaurantbydate(Request $request){
    $date     = $request->date;
    $time     = $request->time;
    $theday   = date("l",strtotime($date));

    $data = $this->restaurant_model->getListRestaurantByOpenDate($theday,$time);
    if($data){
      $return = array(
        "status"    => "200",
        "data"      => $data,
        "total_data"=> $data->count(),
        "params"    => $request->all(),
      );

      return response(json_encode($return), 200);
    }else{
      $return = array(
        "status"  => "404",
      );

      return response(json_encode($return), 400);
    }

  }


  public function getListofRestaurantByLocation(Request $request){
    $longitude          = $request->longitude;
    $latitude           = $request->latitude;
    $current_location   = $request->current_location;
    $user_id            = $request->user_id;

    if($current_location=="true"){
      $user = $this->user_model->getDetail($user_id);
      $longitude    = $user->longitude;
      $latitude     = $user->latitude;
    }

    $data = $this->restaurant_model->getListofRestaurantByLocation($latitude,$longitude);
    if($data){
      $return = array(
        "status"    => "200",
        "data"      => $data,
        "total_data"=> sizeof($data),
        "params"    => $request->all(),
      );

      return response(json_encode($return), 200);
    }else{
      $return = array(
        "status"  => "404",
      );

      return response(json_encode($return), 400);
    }
  }

  public function getListofRestaurantByNumberHour(Request $request){
    $hours          = $request->hours;

    $data = $this->restaurant_model->getListofRestaurantByNumberHour($hours);
    if($data){
      $return = array(
        "status"    => "200",
        "data"      => $data,
        "total_data"=> sizeof($data),
        "params"    => $request->all(),
      );

      return response(json_encode($return), 200);
    }else{
      $return = array(
        "status"  => "404",
      );

      return response(json_encode($return), 400);
    }
  }


  public function getListofRestaurantByDhisesPrice(Request $request){
    $min_price          = $request->min_price;
    $max_price          = $request->max_price;
    $number_of_dishes   = $request->number_of_dishes;

    $data = $this->restaurant_model->getListofRestaurantByDhisesPrice($min_price, $max_price, $number_of_dishes);
    if($data){
      $return = array(
        "status"    => "200",
        "data"      => $data,
        "total_data"=> sizeof($data),
        "params"    => $request->all(),
      );

      return response(json_encode($return), 200);
    }else{
      $return = array(
        "status"  => "404",
      );

      return response(json_encode($return), 400);
    }
  }


  public function getTopUserByTransactionAmount(Request $request){
    $min_date          = $request->min_date;
    $max_date          = $request->max_date;

    $data = $this->user_model->getTopUserByTransactionAmount($min_date, $max_date);
    if($data){
      $return = array(
        "status"    => "200",
        "data"      => $data,
        "total_data"=> sizeof($data),
        "params"    => $request->all(),
      );

      return response(json_encode($return), 200);
    }else{
      $return = array(
        "status"  => "404",
      );

      return response(json_encode($return), 400);
    }
  }

  public function getListofRestaurantByTransactionVolume(Request $request){
    $limit          = $request->limit;

    $data = $this->restaurant_model->getListofRestaurantByTransactionVolume($limit);
    if($data){
      $return = array(
        "status"    => "200",
        "data"      => $data,
        "total_data"=> sizeof($data),
        "params"    => $request->all(),
      );

      return response(json_encode($return), 200);
    }else{
      $return = array(
        "status"  => "404",
      );

      return response(json_encode($return), 400);
    }
  }

  public function getListofRestaurantByDishesRelevance(Request $request){
    $keyword        = $request->keyword;
    $keywords       = explode(" ",$keyword);

    $data = $this->restaurant_model->getListofRestaurantByDishesRelevance($keywords);
    if($data){
      $return = array(
        "status"    => "200",
        "data"      => $data,
        "total_data"=> sizeof($data),
        "params"    => $request->all(),
      );

      return response(json_encode($return), 200);
    }else{
      $return = array(
        "status"  => "404",
      );

      return response(json_encode($return), 400);
    }
  }

  public function getListofRestaurantByDishesName(Request $request){
    $keyword        = $request->keyword;

    $data = $this->restaurant_model->getListofRestaurantByDishesName($keyword);
    if($data){
      $return = array(
        "status"    => "200",
        "data"      => $data,
        "total_data"=> sizeof($data),
        "params"    => $request->all(),
      );

      return response(json_encode($return), 200);
    }else{
      $return = array(
        "status"  => "404",
      );

      return response(json_encode($return), 400);
    }
  }

  public function getUsersWithSpecificTransactionValue(Request $request){
    $min_date          = $request->min_date;
    $max_date          = $request->max_date;
    $limit             = $request->limit;
    $min_transaction   = $request->min_transaction;

    $data = $this->user_model->getUserByTransactionAmountWithMinimumTrx($min_date, $max_date, $min_transaction,$limit);
    if($data){
      $return = array(
        "status"    => "200",
        "data"      => $data,
        "total_data"=> sizeof($data),
        "params"    => $request->all(),
      );

      return response(json_encode($return), 200);
    }else{
      $return = array(
        "status"  => "404",
      );

      return response(json_encode($return), 400);
    }
  }

  public function getListofRestaurantTransactions(Request $request){
    $id_restaurant     = $request->id_restaurant;

    $data = $this->purchase_model->getListofRestaurantTransactions($id_restaurant);
    if($data){
      $return = array(
        "status"    => "200",
        "data"      => $data,
        "total_data"=> sizeof($data),
        "params"    => $request->all(),
      );

      return response(json_encode($return), 200);
    }else{
      $return = array(
        "status"  => "404",
      );

      return response(json_encode($return), 400);
    }
  }

  public function getListofUsersTransactions(Request $request){
    $id_user     = $request->user_id;

    $data = $this->purchase_model->getListofUsersTransactions($id_user);
    if($data){
      $return = array(
        "status"    => "200",
        "data"      => $data,
        "total_data"=> sizeof($data),
        "params"    => $request->all(),
      );

      return response(json_encode($return), 200);
    }else{
      $return = array(
        "status"  => "404",
      );

      return response(json_encode($return), 400);
    }
  }

  public function purchaseThisOrder(Request $request){
    $id_user  = $request->user_id;
    $amount   = $request->amount;

    $user     = $this->user_model->getDetail($id_user);
    if($user->balance<$amount){
      $return = array(
        "status"    => "403",
        "response"  => "Your balance is not enough!"
      );

      return response(json_encode($return), 200);
    }else{
      $arrayupdate = array(
        "id_user"     => $id_user,
        "otp"         => rand(1000,9999),
        "otp_valid"   => time()+60
      );

      $this->user_model->updateData($arrayupdate);

      $return = array(
        "status"      => "200",
        "response"    => "You are about to pay this transaction! We need to confirm if this is you. We send you an OTP code, please use it to confirm your transaction!",
        "otp_sample"  => $arrayupdate['otp']
      );

      return response(json_encode($return), 200);
    }
  }

  public function confirmtoPurchase(Request $request){
    $id_user        = $request->user_id;
    $purchase_code  = $request->purchase_code;
    $otp            = $request->otp;

    $user           = $this->user_model->getDetail($id_user);
    $purchase       = $this->purchase_model->getDetailpurchase($purchase_code);


    if($purchase->payment_status==""){
      if($user->balance<$purchase->subtotal){
        $return = array(
          "status"    => "403",
          "response"  => "Your balance is not enough!"
        );

        return response(json_encode($return), 200);
      }else{
        $saldo_akhir = $user->balance - $purchase->subtotal;

        if($otp==$user->otp && $user->otp_valid>time()){
          //do payment trnsaction here
          //assumes that the payment code here is secure enough and done completely.
          //do payment trnsaction here
          //loging to transaction record here also, we assumes that the payment

          $datapurchase = array(
            "id_purchase"     => $purchase->id_purchase,
            "payment_status"  => "paid"
          );

          $this->purchase_model->updateData($datapurchase);

          $arrayupdate = array(
            "id_user"     => $id_user,
            "balance"     => $saldo_akhir,
            "otp"         => rand(1000,9999)
          );

          $this->user_model->updateData($arrayupdate);

          $return = array(
            "status"      => "200",
            "response"    => "Payment Success",
          );

          return response(json_encode($return), 200);
        }else{
          $return = array(
            "status"    => "403",
            "response"  => "OTP is not valid or expired!"
          );

          return response(json_encode($return), 200);
        }
      }
    }elseif($purchase->payment_status=="paid"){
      $return = array(
        "status"    => "403",
        "response"  => "Invoice has been PAID"
      );

      return response(json_encode($return), 200);
    }
  }

}
