<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('extract-database','ApiController@extractdatabase');

Route::get('api/getListofRestaurantByDate','ApiController@getlistrestaurantbydate');
Route::get('api/getListofRestaurantByLocation','ApiController@getListofRestaurantByLocation');
Route::get('api/getListofRestaurantByNumberHour','ApiController@getListofRestaurantByNumberHour');
Route::get('api/getListofRestaurantByDhisesPrice','ApiController@getListofRestaurantByDhisesPrice');
Route::get('api/getTopUserByTransactionAmount','ApiController@getTopUserByTransactionAmount');
Route::get('api/getListofRestaurantByTransactionVolume','ApiController@getListofRestaurantByTransactionVolume');
Route::get('api/getUsersWithSpecificTransactionValue','ApiController@getUsersWithSpecificTransactionValue');
Route::get('api/getListofRestaurantTransactions','ApiController@getListofRestaurantTransactions');
Route::get('api/getListofUsersTransactions','ApiController@getListofUsersTransactions');
Route::get('api/getListofRestaurantByDishesRelevance','ApiController@getListofRestaurantByDishesRelevance');
Route::get('api/getListofRestaurantByDishesName','ApiController@getListofRestaurantByDishesName');
Route::post('api/purchaseThisOrder','ApiController@purchaseThisOrder');
Route::post('api/confirmtoPurchase','ApiController@confirmtoPurchase');

Route::get('test','ApiController@getMenuByNameOnRestaurant');
