# melalietest

test of melalie project

1. How to import the raw data to the backend
  - put the files of users & restaurant inside folder public/uploads/
  - just execute the URL of http://127.0.0.1:8000/extract-database

1. How to set up and start the backend server (dockerized app is preferred)
  - Put the project file on folder
  - setup and import the database
  - setup .env file for database connection
  - run the project within mainURL/public
  - or you can run locally by "npm run serve" on your command prompt/terminal

3. How to run the test suite (if applicable)
  - jus following the documentation, by using ADVANCE REST or other client service

4. The API documentation
  - here is the API documenttion https://docs.google.com/document/d/19L6FynrqJKEjgOl2pyoF1gLNnxgzm4PwOtlckuGGhOM/edit?usp=sharing

5. The URL to the API
  - test it on https://test.zeals.asia/ + endpoint based on documentation.
